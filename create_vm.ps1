#Required
$VMName = "asd"
$VMRAM = 1024MB
$VMDisk = 5GB
$VMIso = "C:\ua\debian-11.6.0-amd64-netinst2.iso"
$VMGen = 2
$VMSwitch = "internet"

New-VM -Name $VMName -MemoryStartupBytes $VMRAM -BootDevice CD -SwitchName $VMSwitch -Generation $VMGen
New-VHD -Path "C:\VirtualMachines\$VMName\disk.vhdx" -SizeBytes $VMDisk
Add-VMHardDiskDrive -VMName $VMName -Path "C:\VirtualMachines\$VMName\disk.vhdx"
Set-VMDvdDrive -VMName $VMName -Path $VMIso

#Optional
Set-VMFirmware -VMName $VMName -EnableSecureBoot Off
Start-VM -Name $VMName
